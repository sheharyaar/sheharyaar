<head>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/devicons/devicon@v2.14.0/devicon.min.css">
</head>
<p align="center"> <img src="https://gitlab.com/sheharyaar/sheharyaar/-/raw/main/banner-aesthetic.png" alt="wall"/></p>

Hey there! This is Shehar Yaar. A fourth-year undergraduate student at IIT Kharagpur. <br/>
I have knowledge and experience in Web Development, Software Development, Computer Networks and Distributed Systems. <br/>

## Work Experience
1. Software Engineer Intern at [Neverinstall](https://neverinstall.com/) - _1 year 9 months_ (March 2022 - November 2023)
2. Software Engineer Intern at [Securethings](https://securethings.ai/) - _5 months_ (February 2023 - June 2023)


## Skills
<table>
  <tr>
    <td><b>Programming Languages</b></td>
    <td> C, Go, Javascript/Typescript, Python, Rust, Bash Scripting</td>
  </tr>
  <tr>
    <td><b>Libraries and Frameworks</b></td>
    <td> React, NodeJS, FastAPI, POSIX and Linux APIs, Golang Web Frameworks (Gorilla)</td>
  </tr>
  <tr>
    <td><b>Core CSE Skill</b></td>
    <td>Data Structures & Algorithms, Operating System Concepts and Inter-Process Communication (IPC),
Computer Networks and Socket Programming, Distributed Systems, Blockchain</td>
  </tr>
  <tr>
    <td><b>Cloud</b></td>
    <td>AWS, Docker, NGINX, Kubernetes, Git</td>
  </tr>
</table>

## Contributions to Open Source

- Donated my research repo on IIT Kharagpur Network to MetaKGP : [metakgp/iit-kgp-network](https://github.com/metakgp/iit-kgp-network/)
- add cache metrics for NGINX plus : [nginxinc/nginx-prometheus-exporter #540](https://github.com/nginxinc/nginx-prometheus-exporter/pull/540)
- kubearmor/kubearmor-client : [#388](https://github.com/kubearmor/kubearmor-client/pull/388) [#262](https://github.com/kubearmor/kubearmor-client/pull/262)
- kubearmor/KubeArmor : [#899](https://github.com/kubearmor/KubeArmor/pull/899)
- add fetchSysPath : [bendahl/uinput #29](https://github.com/bendahl/uinput/pull/29)
- Remove support for DSA Keys : [libssh/libssh-mirror #231](https://gitlab.com/libssh/libssh-mirror/-/merge_requests/231) 
